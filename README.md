ZeroJS
======

A minimalist Bludit theme with no javascript.

Based upon the [keep-it-simple theme](https://github.com/bludit-themes/keep-it-simple).

<img src="https://code.freedombone.net/bashrc/zerojs/raw/master/preview.png?raw=true" width="80%"/>

Installation
------------

``` bash
cd bl-themes
git clone https://code.freedombone.net/bashrc/zerojs
```

Then within the Bludit admin settings go to **Themes** and activate **ZeroJS**.

Sources and Credits
-------------------

The following resources are used.

Fonts:
 - Open Sans Font (http://www.google.com/fonts/specimen/Open+Sans)
 - Merriweather Font (http://www.google.com/fonts/specimen/Merriweather)

The fonts are included within this repository so that there are no off-site links to Google.

Icons:
 - Font Awesome (http://fortawesome.github.io/Font-Awesome/)

Stock Photos and Graphics:
 - UnSplash.com (http://unsplash.com/)
